# Sing Out (Guitar Agami)
A simple way to save and view the chords and lyrics to a song

Available for use at https://Guitar.Agami.xyz

## Contributing
This was written using the Express framework, Pug templates, and SASS. It uses Yarn as the package manager

To start the server, use `yarn dev`

The SASS will be compiled as needed, using express middleware.
