(function() {
	let songs          = JSON.parse(document.getElementById("available-songs").innerText);
	let elSongList     = document.getElementById("song-list");
	let elSearchBar    = document.getElementById("input-search");
	let elArtistButton = document.getElementById("artist-button");
	let elTitleButton  = document.getElementById("title-button");
	let elRandomButton = document.getElementById("random-button");
	let songObjects    = [];

	let headRow =
		"<span>" +
			"<span class='item'>Title</span>" +
			"<span class='item'>Artist</span>" +
		"</span>";

	function artistSort(array) {
		return array.sort((a, b) => {
			const nameA = a.artist.toUpperCase();
			const nameB = b.artist.toUpperCase(); // Ignore upper and lowercase
			return ((nameA < nameB) ? -1 : 1);
		});
	}
	function titleSort(array) {
		return array.sort((a, b) => {
			const nameA = a.title.toUpperCase();
			const nameB = b.title.toUpperCase(); // Ignore upper and lowercase
			return ((nameA < nameB) ? -1 : 1);
		});
	}
	function randomSort(array) {
		let rand, temp;
		// Loop through array in reverse
		for (let i = array.length - 1; i > 0; i--) {
			// Pick a random remaining element
			rand = Math.floor(Math.random() * (i + 1));
			// Swap it with the current element
			temp = array[i];
			array[i] = array[rand];
			array[rand] = temp;
		}
		return array;
	}

	let sortFunction = artistSort;

	function renderList(searchTerm) {
		let listRows = headRow;
		let artists  = Object.keys(songs);
		songObjects.length = 0;

		for (let artist of artists) {
			for (let title in songs[artist]) {
				if (!searchTerm || search.exact([title, artist], searchTerm)) {
					songObjects.push({ artist, title, id: songs[artist][title] })
				}
			}
		}
		sortFunction(songObjects);
		for (let songObject of songObjects) {
			listRows +=
				"<a href='/song/" + songObject.id[0] + "'>" +
					"<span class='button'>" +
						"<span class='item'>" + escape(songObject.title) + "</span>" +
						"<span class='item'>" + escape(songObject.artist) + "</span>" +
					"</span>" +
				"</a>";
		}
		elSongList.innerHTML = listRows;
	}

	renderList(elSearchBar.value);

	elArtistButton.addEventListener("click", () => {
		elTitleButton.classList.remove("selected");
		elArtistButton.classList.add("selected");
		sortFunction = artistSort;
		renderList(elSearchBar.value);
	});

	elTitleButton.addEventListener("click", () => {
		elArtistButton.classList.remove("selected");
		elTitleButton.classList.add("selected");
		sortFunction = titleSort;
		renderList(elSearchBar.value);
	});

	elRandomButton.addEventListener("click", () => {
		elArtistButton.classList.remove("selected");
		elTitleButton.classList.remove("selected");
		sortFunction = randomSort;
		renderList(elSearchBar.value);
	});

	elSearchBar.addEventListener("keyup", () => {
		renderList(elSearchBar.value);
	});
})();
