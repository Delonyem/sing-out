let song = (function() {
	// Get the unformatted song from the DOM
	let song = JSON.parse(document.getElementById("song-content").innerText);

	// Format the song and add it to the song sheet
	document.getElementById("song").innerHTML = formatSong.defaultToRendered(song, 0);

	// Make the song publically accessable
	return song;
})();

// Populate the versions dropdown
(async function() {
	// Cache DOM
	let elVersionPicker = document.getElementById("version-picker")
	let elVersions = document.getElementById("version-picker-content")

	// GET versions
	let versionsUrl = "/versions/" + encodeURIComponent(song.artists[0]) + "/" + encodeURIComponent(song.title);
	let available = JSON.parse(await httpUtils.get(versionsUrl));

	if (available.length > 1) { // If there are other versions available
		// Show the version picker
		elVersionPicker.style.display = 'block'
		for (let id of available) { // For each available version
			// Add a dropdown item for the version
			elVersions.innerHTML +=
				"<a " +
					"class='drop-item button' " +
					"href='/song/" + id +
				"'>" + id + "</a>";
		}
	}
})();
