let song = (function() {
	let elSongSheet    = document.getElementById("song");
	let elInputTitle   = document.getElementById("input-title");
	let elInputArtist  = document.getElementById("input-artist");
	let elInputContent = document.getElementById("input-content");
	let elInputCaptcha = document.getElementById("input-captcha");
	let elCaptcha      = document.getElementById("captcha");
	let elCaptchaId    = document.getElementById("captcha-id");
	let elCaptchaFail  = document.getElementById("captcha-fail");
	let elSubmit       = document.getElementById("submit");

	let elsCurrentFormat      = document.getElementsByClassName("current-format");
	let elsFormatInfo         = document.getElementsByClassName("formatting-info");
	let elSquareBracketButton = document.getElementById("square-bracketed-format");
	let elSquareBracketInfo   = document.getElementById("square-bracketed-info");
	let elSpacePaddedButton   = document.getElementById("space-padded-format");
	let elSpacePaddedinfo     = document.getElementById("space-padded-info");

	let currentFormat = "squareBracketed";

	// Clear the captcha field on new load
	elInputCaptcha.value = "";

	let song = {
		title: "",
		artists: [""],
		content: ""
	};
	try {
		song = JSON.parse(document.getElementById("song-content").innerText);
		elInputTitle.value   = song.title;
		elInputArtist.value  = song.artists[0];
		elInputContent.value = song.content;
	} catch(e) {
		console.log("No song to edit");
	}

	elSquareBracketButton.addEventListener("click", () => {
		currentFormat = "squareBracketed";
		for (let el of elsCurrentFormat) {
			el.innerText = "Square Bracketed";
		}
		for (let el of elsFormatInfo) {
			el.style.display = "none"
		}
		elSquareBracketInfo.style.display = "block";
		updateSong();
	} );
	elSpacePaddedButton  .addEventListener("click", () => {
		currentFormat = "spacePadded";
		for (let el of elsCurrentFormat) {
			el.innerText = "Space Padded";
		}
		for (let el of elsFormatInfo) {
			el.style.display = "none"
		}
		elSpacePaddedinfo.style.display = "block";
		updateSong();
	} );

	elInputTitle  .addEventListener("keyup", updateSong);
	elInputArtist .addEventListener("keyup", updateSong);
	elInputContent.addEventListener("keyup", updateSong);

	// Submit button
	elSubmit.addEventListener("click", async () => {
		let data = {
			song: {
				title  : elInputTitle.value,
				artists: [elInputArtist.value],
				content: elInputContent.value
			},
			captcha: {
				id  : elCaptchaId.innerText,
				text: elInputCaptcha.value
			}
		};
		if (currentFormat === "spacePadded") {
			data.song.content = formatSong.spacePaddedToDefault(data.song.content);
		}
		try {
			let response = await httpUtils.post("/new/", data);
			console.log(response);
			window.location.href = response;
		} catch(err) {
			try { // Assume a failed captcha
				let newCaptcha = JSON.parse(err);
				elCaptchaId.innerText = newCaptcha.id;
				elCaptcha.innerHTML = newCaptcha.data;
				elInputCaptcha.value = "";
				elCaptchaFail.style = "display: block";
			} catch(err) {
				console.log(err);
			}
		}
	});

	function updateSong() {
		song.title = elInputTitle.value || "Song Title";
		song.artists[0] = elInputArtist.value || "Artist";
		song.content = elInputContent.value || "[A]Example [B]content\nAs you begin to type out the song, this will be replaced.";
		if (currentFormat === "spacePadded") {
			song.content = formatSong.spacePaddedToDefault(song.content);
		}
		elSongSheet.innerHTML = formatSong.defaultToRendered(song, 0);
	}

	updateSong();

	return song;
})();
