let search = {
	/**
	 * Check that at least one of the strings in the array of strings contains the letters in the keyword in order
	 * (there can be other text between the letters)
	 */
	fuzzy: function(data, keyword) {
		for (let i = 0; i < data.length; i ++) {
			let nextIndex = -1;
			let included = true;
			for (let letter of keyword.split('')) {
				nextIndex = data[i].toLowerCase().indexOf(letter.toLowerCase(), nextIndex + 1);
				if (nextIndex === -1) {
					included = false;
					break;
				}
			}
			if (included) { return true; }
		}
		return false;
	},

	/**
	 * Check that at least one of the strings in the array of strings contains the keyword
	 */
	exact: function(data, keyword) {
		for (let i = 0; i < data.length; i ++) {
			// Check the index of the keyword
			let index = data[i].toLowerCase().indexOf(keyword.toLowerCase());
			// If the keyword does in fact have an index, return true
			if (index != -1) {
				return true;
			}
		}
		return false;
	}
};
