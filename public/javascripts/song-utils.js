(function() {
	let transposition = 0;
	let fontSize = 12;
	let elSongSheet = document.getElementById("song");
	elSongSheet.style["font-size"] = fontSize.toString() + "px";

	document.getElementById("transpose-up").addEventListener("click", () => {
		transposition ++;
		elSongSheet.innerHTML = formatSong.defaultToRendered(song, transposition);
	});

	document.getElementById("transpose-down").addEventListener("click", () => {
		transposition += 11;
		elSongSheet.innerHTML = formatSong.defaultToRendered(song, transposition);
	});

	document.getElementById("font-size-up").addEventListener("click", () => {
		fontSize = fontSize / 0.9;
		elSongSheet.style["font-size"] = fontSize.toString() + "px";
	});

	document.getElementById("font-size-down").addEventListener("click", () => {
		fontSize = fontSize * 0.9;
		elSongSheet.style["font-size"] = fontSize.toString() + "px";
	});
})();
