let formatSong = (function() {
	// Store the possible chord letters in an ordered list
	const keys = [
		"A",
		"Bb",
		"B",
		"C",
		"C#",
		"D",
		"Eb",
		"E",
		"F",
		"F#",
		"G",
		"Ab"
	];

	/**
	 *  Replace all the shorthand with valid html and return the complete song html
	 */
	function defaultToRendered(song, transposition) {
		content = escape(song.content);

		// Transpose all the chords to the correct transposition
		content = content.replace(/\[/g,           "[!");
		content = content.replace(/\[\!(A#|Bb)/ig, "[" + keys[(1  + transposition) % 12]);
		content = content.replace(/\[\!(C#|Db)/ig, "[" + keys[(4  + transposition) % 12]);
		content = content.replace(/\[\!(D#|Eb)/ig, "[" + keys[(6  + transposition) % 12]);
		content = content.replace(/\[\!(F#|Gb)/ig, "[" + keys[(9  + transposition) % 12]);
		content = content.replace(/\[\!(G#|Ab)/ig, "[" + keys[(11 + transposition) % 12]);
		content = content.replace(/\[\!A/ig,       "[" + keys[(0  + transposition) % 12]);
		content = content.replace(/\[\!B/ig,       "[" + keys[(2  + transposition) % 12]);
		content = content.replace(/\[\!C/ig,       "[" + keys[(3  + transposition) % 12]);
		content = content.replace(/\[\!D/ig,       "[" + keys[(5  + transposition) % 12]);
		content = content.replace(/\[\!E/ig,       "[" + keys[(7  + transposition) % 12]);
		content = content.replace(/\[\!F/ig,       "[" + keys[(8  + transposition) % 12]);
		content = content.replace(/\[\!G/ig,       "[" + keys[(10 + transposition) % 12]);
		content = content.replace(/\[\!/g,         "[");

		// Seperate the lines into items in an array
		let lines = content.split("\n");
		let formattedSong =
			"<h1>" +
			escape(song.title) +
			"</h1>\n";
		for (let artist of song.artists) {
			formattedSong += "<h2>" + escape(artist) + "</h2>\n";
		}

		for(let line of lines) {
			// Create a line for the chords and one for the lyrics
			let chordLine = "<span class='chord'>";
			let lyricLine = "";
			let chordOffset = 0;

			// Itterate through the line, and put the chords in the chord line
			// and the lyrics in the lyric line
			for (let i = 0; i < line.length; i ++) {
				if (line[i] === "&") { // If the next character is escaped with &...;
					while (line[i] != ";") {
						lyricLine += line[i];
						i ++;
					}
				}
				if (line[i] != "[") { // If it isn't the start of a chord
					// Only add a space to the chord line if the lyric line isn't caught up
					chordOffset ? chordOffset -- :  chordLine += " ";
					lyricLine += line[i];
				} else { // If it is the start of a chord
					while (true) {
						i ++;
						// Keep track of how far ahead of the lyrics the chords are
						chordOffset ++;
						// If it's the end of the chord or the line
						if (line[i] === "]" || !line[i]) {
							// Pad with a space
							chordLine += " ";
							break;
						}
						// Add the chord to the chord line, letter by letter
						chordLine += line[i];
					}
				}
			}

			// Add the newly created lines to the final song text
			formattedSong += chordLine + "</span>\n";
			formattedSong += lyricLine + "\n";
		}

		return formattedSong;
	};

	// Reformat the content of a space padded song to the default format
	function spacePaddedToDefault (songContent) {
		const spaceRegex = / /g;
		const notSpaceRegex = /((?! ).)/g;
		let lines = songContent.split("\n");
		let formattedSong = "";

		for(let i = 0; i < lines.length; i ++) {
			let line = lines[i];
			let formattedLine = "";

			if (line.slice(-1) === "$") { // If it's a chord line
				if (lines[i + 1].slice(-1) !== "$") { // If the next line is a lyric line
					formattedLine = lines[i + 1].padEnd(line.length);
					i ++;
				} else { // There are no lyrics for these chords
					formattedLine = "".padEnd(line.length);
				}

				let chordPosition = 0;
				let chordLine = line.slice(0, -1) + " ";
				for (
					let chordIndex = chordLine.search(notSpaceRegex);
					chordIndex !== -1;
					chordIndex = chordLine.search(notSpaceRegex)
				) {
					chordLine = chordLine.slice(chordIndex);
					chordPosition += chordIndex;
					let chordLength = chordLine.search(spaceRegex);
					formattedLine =
						formattedLine.slice(0, chordPosition) +
						"[" + chordLine.slice(0, chordLength) + "]" +
						formattedLine.slice(chordPosition);
					chordLine = chordLine.slice(chordLength);
					chordPosition += (2 * chordLength) + 2;
				}
			} else { // If it's just a lyrical line
				formattedLine = line;
			}
			// Remove trailing spaces
			formattedLine.replace(/ +$/, "");
			// Add the newly created lines to the final song text
			formattedSong += formattedLine + "\n";
		}

		return formattedSong;
	}

	// Make the functions accessable to the outside
	return { defaultToRendered, spacePaddedToDefault };
})();
