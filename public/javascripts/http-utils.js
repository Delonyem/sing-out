var httpUtils = {
	/**
	 * Make an async http get request to the given url
	 */
	get: function(url) {
		return new Promise(function(resolve, reject) {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					if (this.status == 200) {
						resolve(this.responseText);
					} else {
						reject(this.responseText);
					}
				}
			};
			xhttp.open("GET", url, true);
			xhttp.send();
		});
	},

	/**
	 * Make an async http post request to the given url and data
	 */
	post: function(url, body) {
		return new Promise(function(resolve, reject) {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					if (this.status == 200) {
						resolve(this.responseText);
					} else {
						reject(this.responseText);
					}
				}
			};
			xhttp.open("POST", url, true);
			xhttp.setRequestHeader("Content-type", "application/json")
			xhttp.send(JSON.stringify(body));
		});
	}
};
