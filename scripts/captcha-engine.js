const svgCaptcha = require('svg-captcha');

let activeCaptchas = {};
let old = [];

/** Run to remove any captchas that existed during the last cleanup but were not deleted */
function cleaner() {
	let activeIds = Object.keys(activeCaptchas);
	for (let id of activeIds) {
		let index = old.indexOf(id);
		if (index === -1) { // If the captcha ID is new
			old.push(id);
		} else { // If the captcha ID is old
			old.slice(index, 1);
			delete activeCaptchas[id];
		}
	}
}

/** Make a captcha, and give it an ID */
function create(length = 6, size = 100) {
	var captcha = svgCaptcha.create({
		size: length,
		ignoreChars: "OlI",
		noise: 3,
		width: 0.75 * length * size,
		height: 1.2 * size,
		fontSize: size
	});
	var id = Math.random().toString(36).slice(-8);
	activeCaptchas[id] = captcha.text;
	return [id, captcha.data];
}

/** Test if a captcha ID and string are associated with eachother */
function test(id, text, onlyTest = true) {
	let successText = activeCaptchas[id];
	// Only one test per captcha
	if (onlyTest) { delete activeCaptchas[id]; }

	// If there is recent captcha with the give ID and the text matches it
	if (successText && text === successText) {
		return true;
	} else {
		return false;
	}
}

setInterval(cleaner, 1000 * 60 * 60);

module.exports = { create, test, }
