var fs = require('fs');
var express = require('express');
var router = express.Router();

const songPath = './public/song/';
let availableSongs = {};
let fileSet = [];

function setAvailable() {
	// Store the songs names and IDs in memory
	fs.readdir(songPath, function (err, files) {
		if (err) {
			console.log('Unable to scan directory: ' + err);
			return;
		}
		availableSongs = {};
		// Loop through the file names and update the songs
		for (let file of files) {
			updateSong(file);
		};
	});
}

function updateSong(file) {
	// Make sure the extension is json
	if (!file.endsWith(".json")) { return; }

	// Make not that we've done this file
	fileSet.push(file);

	// Get the artist(s) and title
	fs.readFile(songPath + file, function(err, data) {
		// Stop when there is no file
		if (!data) { return; }
		try {
			// Try to parse the file as JSON
			let song = JSON.parse(data);
			// Add the song to the proper artists
			for (let artist of song.artists) {
				if (!availableSongs[artist]) {
					availableSongs[artist] = {};
				}
				if (availableSongs[artist][song.title]) {
					availableSongs[artist][song.title].push(file.slice(0, -5));
				} else {
					availableSongs[artist][song.title] = [file.slice(0, -5)];
				}
			}
		} catch (err) {
				console.log('Unable parse file: ' + err);
				return;
		}
	});
}

console.log(`Setting songs in ${songPath}`);
setAvailable();

console.log(`Watching for file changes in ${songPath}`);

fs.watch(songPath, function(event, file) {
	if (file && event ==='rename') {
		console.log('File change:', file);
		if (fileSet.includes(file)) {
			setAvailable();
		} else {
			updateSong(file);
		}
	}
});

/* GET home page */
router.get('/', function(req, res, next) {
	res.locals.availableSongs = JSON.stringify(availableSongs);
	res.render('home');
});

/* GET available versions of a specific song */
router.get('/versions/:artist/:title', function(req, res, next) {
	try {
		res.send(availableSongs[req.params.artist][req.params.title]);
	} catch(err) {
		res.send([]);
	}
});

module.exports = router;
