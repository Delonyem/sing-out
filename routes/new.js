var createError = require('http-errors');
var svgCaptcha = require('svg-captcha');
var fs = require('fs');
var crypto = require('crypto');
var express = require('express');
var router = express.Router();
const captchaEngine = require('../scripts/captcha-engine')

let activeCaptchas = {};

/* GET new song page */
router.get('/', function(req, res, next) {
	let [id, data] = captchaEngine.create();

	res.locals.captchaId = id;
	res.locals.captchaData = data;

	res.render('new');
});

/* GET song page */
router.get('/:songId', function(req, res, next) {
	let [id, data] = captchaEngine.create();

	res.locals.captchaId = id;
	res.locals.captchaData = data;

	// Get the song to send with the page
	fs.readFile(`./public/song/${req.params.songId}.json`, function(err, data) {
		if (err) {
			next(createError(404));
			return;
		}
		res.locals.songId = req.params.songId;
		res.locals.songContent = data;
		res.render('new');
	});
});

/* POST a song to the server */
router.post('/', function(req, res, next) {
	try {
		if (!captchaEngine.test(req.body.captcha.id, req.body.captcha.text, true)) {
			let [id, data] = captchaEngine.create();
			res.status(400);
			res.send({ id, data });
			return;
		}

		// Read the song data
		let song = JSON.stringify(req.body.song);

		// Get a shortened sha1 hash of the song for the filename
		var shaSum = crypto.createHash('sha1');
		shaSum.update(song);
		var shaSumText =
			shaSum
				.digest('base64')
				.slice(0, 8)
				.replace(/\+/g, '-')
				.replace(/\//g, '_');

		fs.writeFile('./public/song/' + shaSumText + '.json', song, function (err) {
			if (err) throw err;
			res.send("/song/" + shaSumText);
		});
	} catch (err) {
		next(createError(500));
	}
});

module.exports = router;
