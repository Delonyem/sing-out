var createError = require('http-errors');
var fs = require('fs');
var express = require('express');
var router = express.Router();

/* GET song page */
router.get('/:songId', function(req, res, next) {
	// Get the song to send with the page
	fs.readFile(`./public/song/${req.params.songId}.json`, function(err, data) {
		if (err) {
			next(createError(404));
			return;
		}
		res.locals.songId = req.params.songId;
		res.locals.songContent = data;
		res.render('song');
	});
});

module.exports = router;
